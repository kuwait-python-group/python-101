# conditionals are ways to check for a value or an item

a = ['cheese', 'milk', 'butter', 'eggs']

if 'cheese' in a:
    print('yes')

b = 10
c = 4
d = 6

# less than or if needed <= less than or equal
if b < c:
    print('No')
else:
    print('Yes')

# bigger than or if need >= bigger than or equal
if b > c:
    print('No')
else:
    print('Yes')

# equal to
if b == c:
    print('No')
else:
    print('Yes')

# Not equal
if b != c:
    print('No')
else:
    print('Yes')

if b <= c:
    print('No')
elif d == 6:
    print('Yes to number 6')
else:
    print('No')
