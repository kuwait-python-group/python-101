# Python 101

This tutorial is intended for beginners you can download the latest version of
python from the official python website by clicking [here](https://www.python.org/downloads/)
under the download section and follow the instructions whether on mac or windows.

Alternatively, if you do not like what you see with python idle you can use [anaconda](https://www.anaconda.com/distribution/) or [pycharm](https://www.jetbrains.com/pycharm/) these are alternatives until you find your preference.


# Food for Thought (#exercise, #homework, #challenges)
In the loop section (in the file loops.py) using our supermarket example think of how
would you add a break and a pass in the code

Why not try to add an interesting loop inside a function it doesn't have to be
a mathematical example try it

As for classes can you think of any other interesting ways we can use a class in our
tutorial example the supermarket

if you are interested in answering these questions please do not hesitate to share your
answer with us in code by submitting it here on gitlab
