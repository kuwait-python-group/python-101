# A function is a way to combine many actions in a single process
aisle1 = {}
aisle1["flour"] = 1
aisle1["baking powder"] = 0.1
aisle1["baking soda"] = 0.15
# print aisle1

aisle2 = {}
aisle2["flour"] = 5
aisle2["Rice"] = 2
aisle2["bread"] = 0.500
# print aisle2


def flourPrice():
    """A function than tells us which aisle in the supermarket to go into"""
    if aisle1['flour'] < aisle2['flour']:
        print('Go to aisle1')
    else:
        print('Go ro aisle2')


flourPrice()
