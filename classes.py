# Creating class for our supermarkets
class Supermarket:

    def __init__(self, name, area, size):
        self.name = name
        self.area = area
        self.size = size


# Instantiate the Supermarket object
Salmiya = Supermarket("Salmiya", "Hawally", "Big")
Khaitan = Supermarket("Khaitan", "Farwaniya", "Medium")

# Access the instance attributes
print("{} supermarket is in {} and it's {}. {} supermarket is in {} and it's {}.".format(
    Salmiya.name, Salmiya.area, Salmiya.size, Khaitan.name, Khaitan.area, Khaitan.size))
