# creating a variable
x = 1
y = 'beef'
print(x)

# creating list
a = ['chesse', 'milk', 'butter', 'eggs']
print(a)

# To add an item to list use the append command
a.append('flour')
print(a)

# To remove an item from the list you can use the pop command
# with the number of item we want to remove
a.pop(1)
print(a)

# Tuples are unadjustale items that you can say set in stone
b = ('peanut butter')
print(b)

# Dictionaries are used to assign a value to an item
# we start by creating a disctionary
aisle1 = {}
aisle1["flour"] = 'KD1'
aisle1["baking powder"] = 'KD0.250'
aisle1["baking soda"] = 'KD0.150'
print aisle1

# In order to adjust a dictionary we just need to call the same item with
# new pricing
aisle1['flour'] = 'KD1.100'
print aisle1

# To delete a disctionary we can use del command
del aisle1["baking soda"]
print aisle1
